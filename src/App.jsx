import logo from './logo.svg';
import './App.css';
import React, { useReducer, createContext } from 'react';
import Form from './components/Form';

export const FormContext = createContext();

const appState = {
  isAuthenticated: localStorage.getItem('token') ? true : false,
  token: localStorage.getItem('token'),
  loading: false,
  formFlowList: [
    {
      id: 'dropdown1',
      name: <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>,
      value: '',
      inputType: 'dropdown',
      isOpen: false,
      isEmpty: true,
      prevSentences: 'Saya mau'
    },
    {
      id: 'dropdown2',
      name: <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>,
      value: '',
      inputType: 'dropdown',
      isOpen: false,
      isEmpty: true,
      prevSentences: 'untuk'
    },
    {
      id: 'dropdown3',
      name: <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>,
      value: '',
      inputType: 'dropdown',
      isOpen: false,
      isEmpty: true,
      prevSentences: ''
    },
    {
      id: 'textbox1',
      name: <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>,
      value: '',
      inputType: 'textbox',
      isOpen: false,
      isEmpty: true,
      prevSentences: 'dengan nama'
    }
  ],
  items: [
    {
      id: 'dropdown1',
      data: [
      { label: "Bantuan Notaris", value: "BantuanNotaris" },
      { label: "Bantuan Translator", value: "BantuanTranslator" },
      { label: "Bantuan Pembuatan Website", value: "BantuanPembuatanWebsite" },
      { label: "Perizinan Usaha", value: "PerizinanUsaha" },
      { label: "Virtual Office", value: "VirtualOffice" },
      { label: "Lawyer", value: "Lawyer" }],
    },
    {
      id: 'dropdown2',
      data: [
      { label: "Pendirian", value: "Pendirian" },
      { label: "Perubahan", value: "Perubahan" }]
    },
    {
      id: 'dropdown3',
      data: [
      { label: "PT", value: "PT" },
      { label: "CV", value: "CV" },
      { label: "Firma", value: "Firma" }]
    }
  ]
};

const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN": {
      return null
    }
    case "RELOGIN": {

      return null
    }
    case "LOGOUT": {
      return null
    }
    case "DROPDOWN_TOGGLE": {
      let vals = state.formFlowList;

      vals.forEach(val => {
        if (val.id === action.payload.id) {
          val.isOpen = action.payload.isOpen;
        }
      });
      return {
        ...state,
        formFlowList: vals
      }
    }
    case "DROPDOWN_INPUT": {
      let vals = state.formFlowList;

      vals.forEach(val => {
        if (val.id === action.payload.id) {
          val.name = action.payload.label;
          val.isEmpty = false;
        }
      });
      return {
        ...state,
        formFlowList: vals
      }
    }
    case "TEXT_INPUT": {
      let vals = state.formFlowList;

      vals.forEach(val => {
        if (val.id === action.payload.id) {
          val.name = action.payload.value;
          val.value = action.payload.value;
        }
      });
      return {
        ...state,
        formFlowList: vals
      }
    }
    default:
      return state
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, appState)


  return (
    <React.Fragment>
      <FormContext.Provider value={{
        state,
        dispatch
      }}>
        <Form />
      </FormContext.Provider>
    </React.Fragment>

  );
}

export default App;
