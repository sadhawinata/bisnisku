import React, { useEffect, useState, useContext } from 'react';
import { FormContext } from '../App';

export default function Form() {
    const { state, dispatch } = useContext(FormContext);

    // const [isOpen, setisOpen] = useState(false);

    function generateItem(id) {
        let list = [];

        if (state.items.length > 0) {
            for (let i = 0; i < state.items.length; i++) {
                let items = state.items[i];
                if (items.id === id) {
                    if (items.data.length > 0) {
                        for (let j = 0; j < items.data.length; j++) {
                            let data = items.data[j];
                            list.push(<li key={j} className="fs-5" onClick={() => dropdownInput(data, id)} value={data.value}>{data.label}</li>);
                        }
                    }
                }
            }
        }
        return list;
    }
    function generateFormField(val, fieldName) {
        let content = [];

        if (state.formFlowList.length > 0) {
            for (let idx = 0; idx < state.formFlowList.length; idx++) {
                let form = state.formFlowList[idx];
                
                if (idx === 0) {
                    if (form.inputType === 'dropdown') {
                        content.push(
                            <div className="fade-in word" key={idx}>
                                <div className="fs-2 word">
                                    <label className="fs-2">{form.prevSentences}&nbsp;</label>
                                </div>
                                <div className="fs-2 word field-border" onClick={() => dropdownToggle(!form.isOpen, form.id)}>
                                    {form.name}
                                    <div className={form.isOpen === true ? "fs-2 hidden-word hidden-word-show" : "fs-2 hidden-word"}>
                                        <ul >
                                            {generateItem(form.id)}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
                else {
                    if (state.formFlowList[idx-1].isEmpty) {
                        break;
                    }
                    else {
                        if (form.inputType === 'dropdown') {
                            content.push(
                                <div className="fade-in word" key={idx}>
                                    <div className="fs-2 word">
                                        <label className="fs-2">&nbsp;{form.prevSentences}&nbsp;</label>
                                    </div>
                                    <div className="fs-2 word field-border" onClick={() => dropdownToggle(!form.isOpen, form.id)}>
                                        {form.name}
                                        <div className={form.isOpen === true ? "fs-2 hidden-word hidden-word-show" : "fs-2 hidden-word"}>
                                            <ul>
                                                {generateItem(form.id)}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        else if (form.inputType === 'textbox') {
                            content.push(
                                <div className="fade-in word" key={idx}>
                                    <div className="fs-2 word">
                                        <label className="fs-2">&nbsp;{form.prevSentences}&nbsp;</label>
                                    </div>
                                    <div className="fs-2 word field-border" >
                                        <label onClick={() => dropdownToggle(!form.isOpen, form.id)}>{form.name}</label>
                                        <div className={form.isOpen ? "fs-5 hidden-word hidden-word-show" : "fs-5 hidden-word"}>
                                            <textarea onBlur={() => dropdownToggle(false, form.id)} placeholder="..." name="textbox1" id="textbox1" cols="20" rows="4" value={form.value} onChange={(e)=>textInput(e.target.name, e.target.value, form.id)}></textarea>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    }
                }
            }
        }

        return content;
    }

    function dropdownInput(data, id) {
        let val = {
            ...data,
            id
        }
        dispatch({ type: "DROPDOWN_INPUT", payload: val });
    }

    function dropdownToggle(isOpen, id) {
        let val = {
            isOpen,
            id
        }
        dispatch({ type: "DROPDOWN_TOGGLE", payload: val });
    }

    function textInput(name, value, id) {
        let val = {
            name,
            value,
            id
        }
        dispatch({ type: "TEXT_INPUT", payload: val });
    }
    useEffect(() => {
        // generateFormField();
    }, [state]);
    return (
        <div className="container">
            {/* {isStart ? */}
                <div className="start-now row text-start position-relative">
                    <div className="col-12">
                        <div className="form-group">
                            {generateFormField()}
                        </div>
                    </div>
                    <div className="start-now-footer position-absolute bottom-0 end-0 text-end pb-2 pe-4    ">
                        <button className="btn btn-danger" onClick={()=>window.location.reload()}>Kirim</button>
                    </div>
                </div>
                {/* :
                <div className="row text-start">
                    <div className="col-12">
                        <button className="btn btn-primary" onClick={() => setIsStart(true)}>
                            Start Now
                        </button>
                    </div>
                </div>
            } */}

        </div>
    )
}